# gpdb

#### 介绍
Greenplum数据库是一种大规模并行处理(MPP)数据库服务器，其架构特别针对管理大规模分析型数据仓库以及商业智能工作负载而设计。

MPP(也被称为shared nothing架构)指有两个或者更多个处理器协同执行一个操作的系统，每一个处理器都有其自己的内存、操作系统和磁盘。 Greenplum使用这种高性能系统架构来分布数T字节数据仓库的负载并且能够使用系统的所有资源并行处理一个查询。

Greenplum数据库是基于PostgreSQL开源技术的。它本质上是多个PostgreSQL面向磁盘的数据库实例一起工作形成的一个紧密结合的数据库管理系统(DBMS)。 它基于PostgreSQL 9.4开发，其SQL支持、特性、配置选项和最终用户功能在大部分情况下和PostgreSQL非常相似。 与Greenplum数据库交互的数据库用户会感觉在使用一个常规的PostgreSQL DBMS。

Greenplum数据库可以使用追加优化(append-optimized，AO)的存储格式来批量装载和读取数据，并且能提供HEAP表上的性能优势。 追加优化的存储为数据保护、压缩和行/列方向提供了校验和。行式或者列式追加优化的表都可以被压缩。

Greenplum数据库和PostgreSQL的主要区别在于：

- 在基于Postgres查询规划器的常规查询规划器之外，可以利用GPORCA进行查询规划。
- Greenplum数据库可以使用追加优化的存储。
- Greenplum数据库可以选用列式存储，数据在逻辑上还是组织成一个表，但其中的行和列在物理上是存储在一种面向列的格式中，而不是存储成行。 列式存储只能和追加优化表一起使用。列式存储是可压缩的。当用户只需要返回感兴趣的列时，列式存储可以提供更好的性能。 所有的压缩算法都可以用在行式或者列式存储的表上，但是行程编码(RLE)压缩只能用于列式存储的表。Greenplum数据库在所有使用列式存储的追加优化表上都提供了压缩。

#### 软件架构
软件架构说明

- master：Greenplum数据库的Master是整个Greenplum数据库系统的入口，它接受连接和SQL查询并且把工作分布到Segment实例上。
- segment：Greenplum数据库的Segment实例是独立的PostgreSQL数据库，每一个都存储了数据的一部分并且执行查询处理的主要部分。
- interconnect：Interconect是Greenplum数据库架构中的网络层。Interconnect指的是Segment之间的进程间通信以及这种通信所依赖的网络基础设施。 Greenplum的Interconnect采用了一种标准的以太交换网络。出于性能原因，推荐使用万兆网或者更

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
